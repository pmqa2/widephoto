What Is WidePhoto?
---------------

WidePhoto is a fullscreen theme for photographers that want a photography website that stands out from the rest. Includes a blog custom post type and it's beautifully integrated with the default galleries.

  - Custom Fullscreen Background Slider
  - Custom Blog Post Type
  - Fully integrated with the default galleries

If you have any problems at any time, go to [pqa.me] and check out the support page.

> WidePhoto is a free theme and it is made with great care and love by PQA. You can get > more information about it at [pqa.me]

Setup WidePhoto
---------------
WidePhoto is very easy to use. Recommended values for Media sizes are as follow. On Settings->Media make sure you have this:

  - Thumbnail size - Width: 350 x Height: 175
  - Medium size - Width: 415 x Height: 0
  - Large size - Width: 650 x Height: 0

Problems?
---------------

You can get help at [pqa.me] by clicking on the link at the bottom of the page.

  [pqa.me]: http://pqa.me/
