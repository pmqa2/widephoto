<?php
/**
 * Theme functions
 */

/*--------------------------------------------------
 * Content Width
 *--------------------------------------------------*/
  function widephoto_content_width() {
    $GLOBALS['content_width'] = apply_filters('widephoto_content_width', 650);
  }
  add_action('after_setup_theme', 'widephoto_content_width', 0);

/*--------------------------------------------------
* Theme Setup
*--------------------------------------------------*/

if ( ! function_exists( 'widephoto_setup' ) ):
function widephoto_setup() {

	/*--------------------------------------------------
   * Theme Support
   *--------------------------------------------------*/
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'gallery' ) );
	add_editor_style();

	/*--------------------------------------------------
   * Menus
   *--------------------------------------------------*/
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'widephoto' ),
	) );

	/**
	 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
	 */
	function widephoto_page_menu_args( $args ) {
		$args['show_home'] = true;
		return $args;
	}
	add_filter( 'wp_page_menu_args', 'widephoto_page_menu_args' );

	/*--------------------------------------------------
	 * Translations
	 *--------------------------------------------------*/
	load_theme_textdomain( 'widephoto', get_template_directory() . '/languages' );

	$locale = get_locale();
	$locale_file = get_template_directory() . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );
}
endif; // widephoto_setup
add_action( 'after_setup_theme', 'widephoto_setup' );

/*--------------------------------------------------
 * Includes
 *--------------------------------------------------*/

include get_template_directory() . '/functions/theme-tags.php';

/*--------------------------------------------------
 * Widgets
 *--------------------------------------------------*/
function widephoto_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar 1', 'widephoto' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h1 class="widget-title">',
		'after_title' => '</h1>',
	) );

	register_sidebar( array(
		'name' => __( 'Sidebar 2', 'widephoto' ),
		'id' => 'sidebar-2',
		'description' => __( 'An optional second sidebar area', 'widephoto' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h1 class="widget-title">',
		'after_title' => '</h1>',
	) );
}
add_action( 'init', 'widephoto_widgets_init' );

/*--------------------------------------------------
 * Helpers
 *--------------------------------------------------*/

// adds custom classes to body
function widephoto_body_classes( $classes ) {
	// Adds a class of single-author to blogs with only 1 published author
	if ( ! is_multi_author() ) {
		$classes[] = 'single-author';
	}

	return $classes;
}
add_filter( 'body_class', 'widephoto_body_classes' );

// returns true if a blog has more than 1 category
function widephoto_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so widephoto_categorized_blog should return true
		return true;
	} else {
		// This blog has only 1 category so widephoto_categorized_blog should return false
		return false;
	}
}

// Flush out the transients used in widephoto_categorized_blog
function widephoto_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'widephoto_category_transient_flusher' );
add_action( 'save_post', 'widephoto_category_transient_flusher' );

/**
 * Filter in a link to a content ID attribute for the next/previous image links on image attachment pages
 */
function widephoto_enhanced_image_navigation( $url ) {
	global $post, $wp_rewrite;

	$id = (int) $post->ID;
	$object = get_post( $id );
	if ( wp_attachment_is_image( $post->ID ) && ( $wp_rewrite->using_permalinks() && ( $object->post_parent > 0 ) && ( $object->post_parent != $id ) ) )
		$url = $url . '#main';

	return $url;
}
add_filter( 'attachment_link', 'widephoto_enhanced_image_navigation' );


/*
 * Turns off the default options panel from Twenty Eleven
 */

add_action('after_setup_theme','remove_twentyeleven_options', 100);

function remove_twentyeleven_options() {
	remove_action( 'admin_menu', 'twentyeleven_theme_options_add_page' );
}

/*--------------------------------------------------
 * Scripts
 *--------------------------------------------------*/
function widephoto_enqueue_scripts () {
	// load a specific jquery version
	wp_deregister_script('jquery');
	wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js');
	wp_enqueue_script( 'jquery' );

}
add_action('wp_enqueue_scripts', 'widephoto_enqueue_scripts');

// Add the Supersized script
function widephoto_add_supersized_script () { ?>
		<!-- Supersized scripts -->
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/supersized/css/supersized.css" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/supersized/js/supersized.3.1.3.min.js"></script>

		<script type="text/javascript">

			jQuery(function($){
				$.supersized({

					//Functionality
					slideshow               :   1,		//Slideshow on/off
					autoplay				:	1,		//Slideshow starts playing automatically
					start_slide             :   1,		//Start slide (0 is random)
					slide_interval          :   4000,	//Length between transitions
					slideshow_interval          :   4000,	//Length between transitions
					transition              :   1, 		//0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed		:	500,	//Speed of transition
					new_window				:	1,		//Image links open in new window/tab
					pause_hover             :   0,		//Pause slideshow on hover
					keyboard_nav            :   1,		//Keyboard navigation on/off
					performance				:	1,		//0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
					image_protect			:	1,		//Disables image dragging and right click with Javascript
					image_path				:	'img/', //Default image path

					//Size & Position
					min_width		        :   0,		//Min width allowed (in pixels)
					min_height		        :   0,		//Min height allowed (in pixels)
					vertical_center         :   1,		//Vertically center background
					horizontal_center       :   1,		//Horizontally center background
					fit_portrait         	:   1,		//Portrait images will not exceed browser height
					fit_landscape			:   0,		//Landscape images will not exceed browser width

					//Components
					navigation              :   1,		//Slideshow controls on/off
					thumbnail_navigation    :   1,		//Thumbnail navigation
					slide_counter           :   1,		//Display slide numbers
					slide_captions          :   1,		//Slide caption (Pull from "title" in slides array)
					slides 					:  	[		//Slideshow Images

							<?php
							 $slides = array('wdp_bg_image_1','wdp_bg_image_2','wdp_bg_image_3');
							 $i=1;
							 $j=0;
							 foreach ($slides as $slide) {
									if(get_option($slide)<>"") $j++;
									}
									if ($j != 0) {
										foreach ($slides as $slide) {
												if ($i == $j) {
													echo "{image : '".stripslashes(get_option($slide))."'}";
												}
												else {
													echo "{image : '".stripslashes(get_option($slide))."'},";

												}
										$i++;
										}
									}
									else echo "{image : '".get_template_directory_uri()."/images/before-the-storm.jpg'}, {image : '".get_template_directory_uri()."/images/fisherman.jpg'}, {image : '".get_template_directory_uri()."/images/castle.jpg'}";
								?>
					]

				});
		    });

		</script>
<!-- End of Supersized scripts -->
	<?php
}

add_action('wp_head','widephoto_add_supersized_script');

/*-----------------------------------------------------------------------------------*/
/*	Load Theme Options
/*-----------------------------------------------------------------------------------*/

define('wdp_FILEPATH', TEMPLATEPATH);
define('wdp_DIRECTORY', get_template_directory_uri());

require_once (wdp_FILEPATH . '/admin/admin-functions.php');
require_once (wdp_FILEPATH . '/admin/admin-interface.php');
require_once (wdp_FILEPATH . '/functions/theme-options.php');
require_once (wdp_FILEPATH . '/functions/theme-functions.php');
?>
