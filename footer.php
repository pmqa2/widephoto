<?php
/**
 * The template for displaying the footer.
 */
?>

	</div><!-- #main -->
	<div class="clear"></div>
	</div><!-- #page -->
	<div class="clear"></div>
</div><!-- #wrapper -->

	<footer id="colophon" role="contentinfo">
		<div id="site-generator">
			<?php do_action( 'widephoto_credits' ); ?>
			<div id="credits">&copy;&nbsp;<?php echo date("Y")." ".get_bloginfo('name'); ?> |
			<a href="<?php echo esc_url( __( 'http://pqa.me', 'widephoto' ) ); ?>" title="<?php esc_attr_e( 'Themes', 'widephoto' ); ?>" rel="generator"><?php printf( __( 'Theme by %s', 'widephoto' ), 'PQA' ); ?></a>
		</div>
	</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
